#!/usr/bin/env python3

import asyncio
import sys
import logging
import argparse
from urllib.parse import urljoin
import itertools

import aiocoap
from aiocoap.cli.rd import SimpleRegistrationWKC, RegistrationInterface
from aiocoap.cli.rd import EntityDispatchSite, EndpointLookupInterface
from aiocoap.resource import Site
from aiocoap.util.cli import AsyncCLIDaemon
from aiocoap import error, Message, Context
from aiocoap.cli.common import add_server_arguments
from aiocoap.cli.common import server_context_from_arguments
from aiocoap.util.linkformat import Link, LinkFormat

from influxdb import InfluxDBClient


class LoggerRegistration:
    # FIXME: split this into soft and hard grace period (where the former
    # may be 0). the node stays discoverable for the soft grace period, but
    # the registration stays alive for a (possibly much longer, at least
    # +lt) hard grace period, in which any action on the reg resource
    # reactivates it -- preventing premature reuse of the resource URI
    grace_period = 15

    @property
    def href(self):
        return '/' + '/'.join(self.path)

    def __init__(self, path, network_remote, delete_cb, update_cb,
                 rd, registration_parameters):
        # note that this can not modify d and ep any more, since they were
        # already used in keying to a path
        self.path = path
        self.links = LinkFormat([])

        self._delete_cb = delete_cb
        self._update_cb = update_cb
        self.rd = rd
        self.addr = network_remote.uri
        self.ep = ''
        self.measure = False
        self.update_params(network_remote, registration_parameters,
                           is_initial=True)

    def update_params(self, network_remote, registration_parameters,
                      is_initial=False):
        """Set the registration_parameters from the parsed query arguments,
        update any effects of them, and and trigger any observation
        observation updates if requried (the typical ones don't because
        their registration_parameters are {} and all it does is restart the
        lifetime counter)"""

        if (is_initial or not self.con_is_explicit) and 'con' not in \
                registration_parameters:
            # check early for validity to avoid side effects of requests
            # answered with 4.xx
            try:
                network_con = network_remote.uri
            except error.AnonymousHost:
                raise error.BadRequest("explicit con required")
        self.addr = network_remote.uri

        if is_initial:
            self.registration_parameters = registration_parameters
            self.lt = 86400
            if 'con' not in registration_parameters:
                self.con = network_con
                self.con_is_explicit = False
            self.ep = registration_parameters['ep']

            # technically might be a re-registration, but we can't catch
            # that at this point
            actual_change = True
        else:
            if 'd' in registration_parameters or 'ep'\
                    in registration_parameters:
                raise error.BadRequest("Parameters 'd' and 'ep' can not be"
                                       "updated")

            actual_change = any(v != self.registration_parameters[k] for
                                (k, v) in registration_parameters.items())

            self.registration_parameters = dict(self.registration_parameters,
                                                **registration_parameters)

        logging.warning("Update for {}".format(self.ep))
        if 'lt' in registration_parameters:
            try:
                self.lt = int(registration_parameters['lt'])
            except ValueError:
                raise error.BadRequest("lt must be numeric")

        if 'con' in registration_parameters:
            self.con = registration_parameters['con']
            self.con_is_explicit = True

        if not self.con_is_explicit and self.con != network_con:
            self.con = network_con
            actual_change = True

        if is_initial:
            self._set_timeout()
        else:
            self.refresh_timeout()

        if not self.measure:
            logging.warning("Measure: {}".format(self.measure))
            self._start_measure()

        if actual_change:
            self._update_cb()

    def delete(self):
        logging.warning("Deleting registration {}".format(self.ep))
        self.timeout.cancel()
        self.measure = False
        self._update_cb()
        self._delete_cb()

    def _set_timeout(self):
        delay = self.lt + self.grace_period
        # workaround for python issue20493

        async def longwait(delay, callback):
            almostday = 24*60*60 - 10
            while delay > almostday:
                await asyncio.sleep(almostday)
                delay -= almostday
            await asyncio.sleep(delay)
            callback()
        self.timeout = asyncio.Task(longwait(delay, self.delete))

    async def push_message(self, uri, data):
        target = self.addr + uri
        protocol = await Context.create_client_context()
        request = Message(code=aiocoap.POST, uri=target, payload=data)
        try:
            await protocol.request(request).response
        except Exception as e:
            logging.error("Failed to push message to"
                          " {}/{}: {}".format(self.addr, uri, e))

    def pub_measurement(self, link, payload):
        try:
            val = float(payload)
        except Exception as e:
            logging.error("Failed to decode payload: {}".format(e))
        else:
            influx = self.rd.get_influx()
            body = [
                    {
                        "measurement": "points",
                        "tags": {
                            "host": self.ep,
                            "url": link.href,
                        },
                        "fields": {
                            "value": val,
                        }
                    }
                ]
            try:
                influx.write_points(body)
            except Exception as e:
                logging.error("Failed to push data: {}".format(e))

    async def get_measurement(self, link):
        uri = link.get_target(self.addr)
        protocol = await Context.create_client_context()
        request = Message(code=aiocoap.GET, uri=uri)
        try:
            response = await protocol.request(request).response
        except Exception as e:
            logging.error('Failed to fetch resource: {}'.format(e))
        else:
            try:
                payload = response.payload.decode("utf-8")
                logging.warning('Result: {}-{}: {}'.format(self.ep, link.href,
                                                           payload))
                self.pub_measurement(link, response.payload)

            except Exception as e:
                logging.error("Failed to decode response: {}".format(e))
        await protocol.shutdown()

    def _start_measure(self):

        async def measure(self):
            self.measure = True;
            while self.measure:
                for link in self.links.links:
                    if str(link).startswith("</s/"):
                        asyncio.ensure_future(self.get_measurement(link))
                await asyncio.sleep(30)
            logging.warning("Exiting measurements on {}".format(self.ep))

        logging.warning("Starting measurements on {} - {}".format(self.ep, self.addr))
        asyncio.ensure_future(measure(self))

    def refresh_timeout(self):
        self.timeout.cancel()
        self._set_timeout()

    def get_host_link(self):
        args = dict(self.registration_parameters, con=self.con)
        return Link(href=self.href, **args)

    def get_conned_links(self):
        """Produce a LinkFormat object that represents all statements in
        the registration, resolved to the registration's con (and thus
        suitable for serving from the lookup interface).

        If protocol negotiation is implemented and con becomes a list, this
        function will probably grow parameters that hint at which con to
        use.
        """
        result = []
        for l in self.links.links:
            if 'anchor' in l:
                absanchor = urljoin(self.con, l.anchor)
                data = [(k, v) for (k, v) in l.attr_pairs if
                        k != 'anchor'] + [['anchor', absanchor]]
                href = urljoin(absanchor, l.href)
            else:
                data = l.attr_pairs + [['anchor', self.con]]
                href = urljoin(self.con, l.href)
            result.append(Link(href, data))
        return LinkFormat(result)


class CustomRD:
    # the `key` of an endpoint is not really worked out yet. currently it's
    # ep; code that handles key internals should be limited to
    # this class.

    entity_prefix = ("reg",)

    def __init__(self):
        super().__init__()

        self._endpoint_registrations_by_key = {}  # key -> Registration
        self._entities_by_pathtail = {}  # path -> Registration or Group
        self._updated_state_cb = []
        self.mqtt = None
        self.influx = InfluxDBClient("iot.snt.utwente.nl", 8086, "dmn",
                                     "nui4ilae5OoNei7io1ETi8cee3shoo1i", "dmn")

    async def shutdown(self):
        pass

    def get_influx(self):
        return self.influx

    def register_change_callback(self, callback):
        """Ask RD to invoke the callback whenever any of the RD state
        changed"""
        self._updated_state_cb.append(callback)

    def _updated_state(self):
        for cb in self._updated_state_cb:
            cb()

    def _new_pathtail(self):
        for i in itertools.count(1):
            # In the spirit of making legal but unconvential choices (see
            # StandaloneResourceDirectory documentation): Whoever strips or
            # ignores trailing slashes shall have a hard time keeping
            # registrations alive.
            path = (str(i), '')
            if path not in self._entities_by_pathtail:
                return path

    def endpoint_by_key(self, key):
        if key in self._endpoint_registrations_by_key:
            return self._endpoint_registrations_by_key[key]
        else:
            return None

    def initialize_endpoint(self, network_remote, registration_parameters):
        try:
            ep = registration_parameters['ep']
        except KeyError:
            raise error.BadRequest("ep argument missing")

        key = ep

        try:
            oldreg = self._endpoint_registrations_by_key[key]
        except KeyError:
            path = self._new_pathtail()
        else:
            path = oldreg.path[len(self.entity_prefix):]
            oldreg.delete()

        # this was the brutal way towards idempotency (delete and re-create).
        # if any actions based on that are implemented here, they have yet to
        # decide wheter they'll treat idempotent recreations like deletions or
        # just ignore them unless something otherwise unchangeable (ep, d)
        # changes.

        def delete():
            del self._entities_by_pathtail[path]
            del self._endpoint_registrations_by_key[key]

        reg = LoggerRegistration(self.entity_prefix + path,
                                 network_remote, delete,
                                 self._updated_state, self,
                                 registration_parameters)

        self._endpoint_registrations_by_key[key] = reg
        self._entities_by_pathtail[path] = reg

        return reg

    def get_endpoints(self):
        return self._endpoint_registrations_by_key.values()


class CustomResourceDirectory(Site):
    """A site that contains all function sets of the CoAP Resource Directory"""

    rd_path = ("resourcedirectory",)
    group_path = ("resourcedirectory-group",)
    ep_lookup_path = ("endpoint-lookup",)
    gp_lookup_path = ("group-lookup",)
    res_lookup_path = ("resource-lookup",)

    def __init__(self):
        super().__init__()

        common_rd = CustomRD()

        resources = self.get_resources_as_linkheader
        self._simple_wkc = SimpleRegistrationWKC(resources,
                                                 common_rd=common_rd)
        self.add_resource((".well-known", "core"), self._simple_wkc)

        self.add_resource(self.rd_path,
                          RegistrationInterface(common_rd=common_rd))
        self.add_resource(common_rd.entity_prefix,
                          EntityDispatchSite(common_rd=common_rd))
        self.add_resource(self.ep_lookup_path,
                          EndpointLookupInterface(common_rd=common_rd))

        self.common_rd = common_rd

    async def shutdown(self):
        await self.common_rd.shutdown()

    # the need to pass this around crudely demonstrates that the setup of sites
    # and contexts direly needs improvement, and thread locals are giggling
    # about my stubbornness
    def set_context(self, new_context):
        self._simple_wkc.context = new_context


def build_parser():
    p = argparse.ArgumentParser(description=__doc__)

    add_server_arguments(p)

    return p


class Main(AsyncCLIDaemon):
    async def start(self, args=None):
        parser = build_parser()
        options = parser.parse_args(args if args is not None else sys.argv[1:])

        self.site = CustomResourceDirectory()

        self.context = await server_context_from_arguments(self.site, options)
        self.site.set_context(self.context)

    async def shutdown(self):
        await self.site.shutdown()
        await self.context.shutdown()


_sync_main = Main.sync_main

if __name__ == "__main__":
    _sync_main()
